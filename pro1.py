from tkinter import *
#from PIL import Image, ImageTk
me_root = Tk()

# The default size of our window and dimension of max and min of it.
me_root.geometry("500x500")
me_root.minsize(100,100)
me_root.maxsize(800,800)

# adding text in that window.
blacky = Label (text = "People in black looks the most beautiful")
blacky.pack()

# adding photo in that window.
photo = PhotoImage (file = "1.png")
ramya_label = Label(image = photo)
ramya_label.pack()

# adding photo in window other than png formats.
#image = Image.open("1.jpeg")
#photo = ImageTk.PhotoImage(image)
#photo.pack()

me_root.mainloop()

