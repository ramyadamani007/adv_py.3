def Div(n):
	for j in range(1, n+1):
		if j % 2 == 0:
			yield j
 
if __name__ == "__main__":
	x= int (input ("Enter a value upto which you want to print integers that are divisible by 7.\n"))
	li = []				
	for j in Div(x):
		li.append(j)
	print (li)
